//
//  Account.swift
//  StackTrackrApp
//
//  Created by George McLoughlin on 11/26/23.
//

import Foundation
import FirebaseFirestoreSwift
import Firebase

struct Account: Codable, Identifiable {
    @DocumentID var id: String?
    var accountId: String           // Plaid account ID
    var availableBalance: Double    // Available balance
    var currentBalance: Double      // Current balance
    var isoCurrencyCode: String     // ISO currency code
    var name: String                // Account name
    var officialName: String        // Official name of the account
    var subtype: String             // Account subtype
    var type: String                // Account type
    var limit: Double

    // Initializer
    init(accountId: String, availableBalance: Double, currentBalance: Double, isoCurrencyCode: String, name: String, officialName: String, subtype: String, type: String, limit: Double) {
        self.accountId = accountId
        self.availableBalance = availableBalance
        self.currentBalance = currentBalance
        self.isoCurrencyCode = isoCurrencyCode
        self.name = name
        self.officialName = officialName
        self.subtype = subtype
        self.type = type
        self.limit = limit
    }
    
}

func addAccount(userId: String, account: Account) {
    let firestore = Firestore.firestore()
    let accountsRef = firestore.collection("users").document(userId).collection("accounts")

    do {
        try accountsRef.addDocument(from: account) { error in
            if let error = error {
                // Handle the error - could not write the document
                print("Error writing account to Firestore: \(error)")
            } else {
                // The account was successfully written to Firestore
                print("Account successfully added")
            }
        }
    } catch let error {
        // Handle any errors in the account object itself (e.g., serialization error)
        print("Error adding account: \(error)")
    }
}

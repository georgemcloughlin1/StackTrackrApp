//
//  ServerCommunicator.swift
//  StackTrackrApp
//
//  Created by George McLoughlin on 11/26/23.
//

import Foundation
import FirebaseAuth

class ServerCommunicator {

    enum HTTPMethod: String {
        case get = "GET"
        case post = "POST"
    }

    enum Error: LocalizedError {
        case invalidUrl(String)
        case networkError(String, Data?) // Include Data? to store raw response data
        case encodingError(String)
        case decodingError(String)
        case nilData

        var localizedDescription: String {
            switch self {
            case .invalidUrl(let url): return "Invalid URL: \(url)"
            case .networkError(let error, _): return "Network Error: \(error)"
            case .encodingError(let error): return "Encoding Error: \(error)"
            case .decodingError(let error): return "Decoding Error: \(error)"
            case .nilData: return "Server returned null data"
            }
        }

        var errorDescription: String? {
            return localizedDescription
        }

        // Add a new property to get the raw response data
        var responseData: Data? {
            switch self {
            case .networkError(_, let data):
                return data
            default:
                return nil
            }
        }
    }



    init(baseURL: String = "https://us-central1-stacktrackr-ed8d3.cloudfunctions.net/api/") {
        self.baseURL = baseURL
    }

    func callMyServer<T: Decodable>(
        url: String? = nil,
        path: String,
        httpMethod: HTTPMethod,
        params: [String: Any]? = nil,
        token: String? = nil,
        completion: @escaping (Result<T, ServerCommunicator.Error>) -> Void) {

            let path = path.hasPrefix("/") ? String(path.dropFirst()) : path
            var urlString = ""
            if (url == nil){
                urlString = baseURL + path
            }else{
                urlString = url ?? "" + path
                print("custom/dev url used")
            }
            print(urlString)
            
            
            guard let url = URL(string: urlString) else {
                completion(.failure(ServerCommunicator.Error.invalidUrl(urlString)))
                return
            }

            var request = URLRequest(url: url)
            request.httpMethod = httpMethod.rawValue
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            if let token = token {
                let authHeaderValue = "Bearer \(token)"
                request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
            
            print("URL: \(request.url?.absoluteString ?? "None")")
            print("HTTP Method: \(request.httpMethod ?? "None")")

            // Print Headers
            if let headers = request.allHTTPHeaderFields {
                print("Headers:")
                for (header, value) in headers {
                    print("\(header): \(value)")
                }
            }

            // Print Body if it exists
            if let body = request.httpBody {
                if let bodyString = String(data: body, encoding: .utf8) {
                    print("Body: \(bodyString)")
                }
            }
            


            switch httpMethod {
            case .post where params != nil:
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: params!, options: [])
                    request.httpBody = jsonData
                } catch {
                    completion(.failure(.encodingError("\(error)")))
                    return
                }
            default:
                break
            }

            // Create the task
            let task = URLSession.shared.dataTask(with: request) { (data, _, error) in
                DispatchQueue.main.async {
                    if let error = error {
                        // Even if there's an error, if there's also data, it might be the HTML you're looking for
                        print("Error: \(error.localizedDescription)")
                        if let data = data, let rawResponse = String(data: data, encoding: .utf8) {
                            print("Raw server response: \(rawResponse)")
                        }
                        return
                    }

                    // Handle the case where there's no data
                    guard let data = data else {
                        print("No data received from server")
                        return
                    }

                    // Try to decode the data, and handle any decoding errors
                    do {
                        let object = try JSONDecoder().decode(T.self, from: data)
                        // Handle the success case
                    } catch {
                        print("Decoding Error: \(error.localizedDescription)")
                        if let rawResponse = String(data: data, encoding: .utf8) {
                            print("Raw server response: \(rawResponse)")
                        }
                    }
                }
            }

            task.resume()

    }
    
    struct DummyDecodable: Decodable { }

    // Convenience method where we don't want to do anything yet with the result, beyond seeing what we get back from the server
    func callMyServer(
        path: String,
        httpMethod: HTTPMethod,
        params: [String: Any]? = nil
    ) {
        callMyServer(path: path, httpMethod: httpMethod, params: params) { (_: Result<DummyDecodable, ServerCommunicator.Error>) in
            // Do nothing here
        }
    }


    private let baseURL: String
}

extension Data {

    fileprivate func printJson() {
        do {
            let json = try JSONSerialization.jsonObject(with: self, options: [])
            let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            guard let jsonString = String(data: data, encoding: .utf8) else {
                print("Invalid data")
                return
            }
            print(jsonString)
        } catch {
            print("Error: \(error.localizedDescription)")
        }
    }
}

func fetchFirebaseIDToken(completion: @escaping (Result<String, Error>) -> Void) {
    guard let user = Auth.auth().currentUser else {
        completion(.failure(NSError(domain: "AuthError", code: -1, userInfo: [NSLocalizedDescriptionKey: "User is not logged in."])))
        return
    }

    user.getIDToken { token, error in
        if let error = error {
            completion(.failure(error))
            return
        }

        guard let token = token else {
            completion(.failure(NSError(domain: "TokenError", code: -1, userInfo: [NSLocalizedDescriptionKey: "ID token is nil."])))
            return
        }

        completion(.success(token))
    }
}

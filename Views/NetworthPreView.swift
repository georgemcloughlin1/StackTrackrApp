import Foundation
import SwiftUI
import FirebaseFirestoreSwift
import Firebase

struct NetworthPreView: View {
    @StateObject var viewModel = NetworthViewModel()
    
    var body: some View {
        VStack {
            if viewModel.hasNetworthAccounts {
                // TODO: Display networth details
            } else {
                Text("You have no linked networth accounts.")
                Button("Link Account") {
//                    viewModel.fetchLinkToken()
                    viewModel.testConnection()
                }
                Button("Test Connection"){
                    viewModel.testConnection()
                }
                // TODO: Consider adding .disabled(viewModel.isFetching) to disable the button during fetch
            }
        }
    }
}

class NetworthViewModel: ObservableObject {
    @Published var accounts: [Account] = []
    @Published var hasNetworthAccounts = false
    @Published var isLinkActive = false
    @Published var linkToken: String?
    private var communicator = ServerCommunicator()
    
    func fetchAccounts(userId: String) {
        let firestore = Firestore.firestore()
        firestore.collection("users").document(userId).collection("accounts").getDocuments { [weak self] (snapshot, error) in
            if let error = error {
                print("Error getting accounts: \(error)")
                self?.hasNetworthAccounts = false
                return
            }
            
            guard let documents = snapshot?.documents else {
                self?.hasNetworthAccounts = false
                return
            }
            
            self?.accounts = documents.compactMap { document in
                try? document.data(as: Account.self)
            }
            
            // Check specifically for 'networth' type accounts
            self?.hasNetworthAccounts = self?.accounts.contains(where: { $0.type == "networth" }) ?? false
        }
    }
    
    func fetchLinkToken() {
        guard let userId = Auth.auth().currentUser?.uid else {
            print("User is not logged in.")
            return
        }
        
        let user: [String: Any] = ["userId": userId]
        
        fetchFirebaseIDToken { [weak self] result in
            switch result {
            case .success(let token):
                // Now that you have the token, call your server function with the token
                self?.communicator.callMyServer(url: "https://generatelinktoken-jdka7uxk2q-uc.a.run.app",path: "", httpMethod: .post, params: user, token: token) { (result: Result<LinkTokenCreateResponse, ServerCommunicator.Error>) in
                    DispatchQueue.main.async {
                        switch result {
                        case .success(let response):
                            self?.linkToken = response.linkToken
                            self?.isLinkActive = true
                        case .failure(let error):
                            print("Error fetching link token: \(error)")
                        }
                    }
                }
                
            case .failure(let error):
                print("Error fetching ID token: \(error.localizedDescription)")
                // Handle the error here
            }
        }
        

    }
    
    func testConnection() {
        self.communicator.callMyServer(url: "https://testconnection-jdka7uxk2q-uc.a.run.app/", path: "", httpMethod: .get) { (result: Result<String, ServerCommunicator.Error>) in
            switch result {
            case .success(let response):
                print("Response from server: \(response)")
            case .failure(let error):
                print("Error contacting server: \(error)")
            }
        }
    }

}


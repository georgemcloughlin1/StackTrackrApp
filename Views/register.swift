import SwiftUI

struct RegisterPage: View {
    @Environment(\.dismiss) var dismiss
    @State private var email: String = ""
    @State private var phoneNumber: String = ""
    @State private var password: String = ""
    @State private var passwordConfirm: String = ""
    @State private var fName: String = ""
    @State private var lName: String = ""
    @EnvironmentObject var sessionStore: SessionStore
    @StateObject private var phoneNumberFormatter = PhoneNumberFormatter()

    
    var body: some View {
        NavigationView {
            VStack(spacing: 20) {
                TextField("Email", text: $email)
                    .keyboardType(.emailAddress)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .padding()
                    .background(Color(.systemGray6))
                    .cornerRadius(5)
                
                TextField("First Name", text: $fName)
                    .keyboardType(.emailAddress)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .padding()
                    .background(Color(.systemGray6))
                    .cornerRadius(5)
                
                TextField("Last Name", text: $lName)
                    .keyboardType(.emailAddress)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .padding()
                    .background(Color(.systemGray6))
                    .cornerRadius(5)

                TextField("Phone Number", text: $phoneNumberFormatter.phoneNumber)
                    .onChange(of: phoneNumberFormatter.phoneNumber) {
                        phoneNumberFormatter.formatPhoneNumber($0)
                    }
                    .keyboardType(.phonePad)
                    .padding()
                    .background(Color(.systemGray6))
                    .cornerRadius(5)
                
                SecureField("Password", text: $password)
                    .padding()
                    .background(Color(.systemGray6))
                    .cornerRadius(5)

                SecureField("Confirm Password", text: $passwordConfirm)
                    .padding()
                    .background(Color(.systemGray6))
                    .cornerRadius(5)
                
                Button(action: {
                    sessionStore.signUp(email: email, password: password, displayName: fName + " " + lName, phoneNumber: phoneNumber)
                    print("Register button tapped!")
                }) {
                    Text("Register")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 50)
                        .foregroundColor(.white)
                        .font(.system(size: 14, weight: .bold))
                        .background(Color.blue)
                        .cornerRadius(5)
                }

                Spacer()

                Button("Back to Login") {
                    dismiss()
                }
                .foregroundColor(.blue)
            }
            .padding()
            .navigationTitle("Register")
        }
    }
}

class PhoneNumberFormatter: ObservableObject {
    @Published var phoneNumber: String = ""

    private var rawNumber: String {
        phoneNumber.filter { "0"..."9" ~= $0 }
    }

    func formatPhoneNumber(_ number: String) {
        // Remove any existing formatting
        var rawNumber = number.filter { "0"..."9" ~= $0 }

        // Apply custom formatting
        if rawNumber.count > 3 {
            let startIndex = rawNumber.index(rawNumber.startIndex, offsetBy: 3)
            rawNumber.insert("-", at: startIndex)
        }
        if rawNumber.count > 7 {
            let startIndex = rawNumber.index(rawNumber.startIndex, offsetBy: 7)
            rawNumber.insert("-", at: startIndex)
        }
        
        // Truncate if longer than 12 characters (3-3-4 plus two hyphens)
        if rawNumber.count > 12 {
            rawNumber = String(rawNumber.prefix(12))
        }

        self.phoneNumber = rawNumber
    }
}

struct RegisterPage_Previews: PreviewProvider {
    static var previews: some View {
        RegisterPage()
    }
}


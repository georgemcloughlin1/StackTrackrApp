//
//  SettingsView.swift
//  StackTrackrApp
//
//  Created by George McLoughlin on 11/5/23.
//

import Foundation
import SwiftUI
import FirebaseAuth

struct SettingsView: View {
    @EnvironmentObject var sessionStore: SessionStore

    var body: some View {
        NavigationView {
            List {
                Section(header: Text("Account")) {
                    Button(action: {
                        sessionStore.signOut()
                    }) {
                        HStack {
                            Text("Sign Out")
                            Spacer()
                            Image(systemName: "arrow.right.circle.fill")
                        }
                    }

                    NavigationLink(destination: ForgotPasswordView()) {
                        HStack {
                            Text("Change Password")
                            Spacer()
                            Image(systemName: "arrow.right.circle.fill")
                        }
                    }
                }
                // Additional settings can be added here as new List items
            }
            .navigationTitle("Settings")
        }
    }
}




//  BudgetGraphView.swift
//  StackTrackrApp
//
//  Created by George McLoughlin on 12/11/23.
//

import Foundation
import SwiftUI
import Charts

class BudgetCategory: Identifiable, ObservableObject {
    let id = UUID()
    @Published var name: String
    @Published var budget: Double
    @Published var spent: Double
    
    init(name: String, budget: Double, spent: Double) {
        self.name = name
        self.budget = budget
        self.spent = spent
    }
}

class moneyTransaction: Identifiable, ObservableObject {
    let id = UUID()
    @Published var merchantName: String
    @Published var cost: Double
    @Published var date: Date
    @Published var merchantCategory: String
    @Published var logoUrl: String // photo url
    @Published var creditCard: CreditCard
    
    init (merchantName: String, cost: Double, date: Date, merchantCategory: String, logoUrl: String, creditCard: CreditCard) {
        self.merchantName = merchantName
        self.cost = cost
        self.date = date
        self.merchantCategory = merchantCategory
        self.logoUrl = logoUrl
        self.creditCard = creditCard
    }
}

let mockCreditCards = [
    CreditCard(name: "Savor One", institution: "Bank A", balance: 1200.0, limit: 5000.0, color: .red),
    CreditCard(name: "BoFa Deez", institution: "Bank B", balance: 3500.0, limit: 7000.0, color: .green),
    CreditCard(name: "Black", institution: "Bank C", balance: 500.0, limit: 3000.0, color: .blue),
    CreditCard(name: "Apple", institution: "Bank D", balance: 2500.0, limit: 6000.0, color: .yellow)
]

let specificDate = Calendar.current.date(from: DateComponents(year: 2023, month: 12, day: 17))!

var mockTransactions = [
    moneyTransaction(merchantName: "Store A", cost: 45.99, date: specificDate, merchantCategory: "Groceries", logoUrl: "https://example.com/logo1.png", creditCard: mockCreditCards[0]),
    moneyTransaction(merchantName: "Cafe B", cost: 12.50, date: Date(), merchantCategory: "Dining", logoUrl: "https://example.com/logo2.png", creditCard: mockCreditCards[1]),
    moneyTransaction(merchantName: "Electronics C", cost: 129.99, date: specificDate, merchantCategory: "Electronics", logoUrl: "https://example.com/logo3.png", creditCard: mockCreditCards[0]),
    moneyTransaction(merchantName: "Bookstore D", cost: 23.75, date: Date(), merchantCategory: "Books", logoUrl: "https://example.com/logo4.png", creditCard: mockCreditCards[2]),
    moneyTransaction(merchantName: "Gym E", cost: 60.00, date: specificDate, merchantCategory: "Fitness", logoUrl: "https://example.com/logo5.png", creditCard: mockCreditCards[1]),
    moneyTransaction(merchantName: "Clothing F", cost: 80.20, date: Date(), merchantCategory: "Apparel", logoUrl: "https://example.com/logo6.png", creditCard: mockCreditCards[3])
]

func groupTransactions(transactions: [moneyTransaction]) -> [Date: [String: [moneyTransaction]]] {
    let groupedByDate = Dictionary(grouping: transactions) { $0.date.startOfDay }
    
    var groupedByDateAndCard: [Date: [String: [moneyTransaction]]] = [:]
    
    for (date, transactions) in groupedByDate {
        let groupedByCard = Dictionary(grouping: transactions) { $0.creditCard.name }
        groupedByDateAndCard[date] = groupedByCard
    }
    
    return groupedByDateAndCard
}

extension Date {
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
}

struct TransactionListView: View {
    var showTransactionView: Bool = true
    var transactions: [moneyTransaction]
    
    // Grouped transactions for the view
    var groupedTransactions: [Date: [String: [moneyTransaction]]] {
        groupTransactions(transactions: transactions)
    }
    
    var body: some View {
        NavigationView {
            ScrollView {
                LazyVStack(spacing: 10) {
                    ForEach(groupedTransactions.keys.sorted(by: >), id: \.self) { date in
                        SectionHeaderView(date: date)
                        ForEach(groupedTransactions[date]?.keys.sorted() ?? [], id: \.self) { cardName in
                            CardView(cardName: cardName, transactions: groupedTransactions[date]?[cardName] ?? [])
                        }
                    }
                }
            }
        }
    }
}

struct SectionHeaderView: View {
    let date: Date
    
    var body: some View {
        HStack {
            Text(date, formatter: itemFormatter)
                .font(.system(size: 16, weight: .semibold))
                .foregroundColor(.white)
            Spacer()
        }
        .padding(.vertical, 8)
        .padding(.horizontal)
        .background(Color.gray.opacity(0.2))
        .listRowInsets(EdgeInsets())
    }
}

struct CardView: View {
    let cardName: String
    let transactions: [moneyTransaction]
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text(cardName)
                .font(.headline)
                .padding(.top, 5)
            ForEach(transactions, id: \.id) { transaction in
                TransactionRow(transaction: transaction)
            }
        }
        .padding()
        .background(Color.white)
        .cornerRadius(10)
        .shadow(color: .gray.opacity(0.2), radius: 5, x: 0, y: 2)
        .padding(.horizontal)
    }
}

struct TransactionRow: View {
    var transaction: moneyTransaction
    
    var body: some View {
        NavigationLink(destination: TransactionDetailView(transaction: transaction)) {
            // ... your existing code for the row content
            
            HStack {
                VStack(alignment: .leading, spacing: 4) {
                    Text(transaction.merchantName).fontWeight(.medium)
                    Text(transaction.merchantCategory).font(.subheadline).foregroundColor(.secondary)
                }
                Spacer()
                Text(currencyFormatter.string(from: NSNumber(value: transaction.cost)) ?? "")
                    .foregroundColor(transaction.cost < 0 ? .red : .primary)
            }
        }
    }
}

struct TransactionDetailView: View {
    var transaction: moneyTransaction
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            Text("Merchant: \(transaction.merchantName)")
            Text("Category: \(transaction.merchantCategory)")
           // Text("Amount: \(transaction.cost, formatter: currencyFormatter)")
            // Add more transaction details here as needed
        }
        .padding()
        .navigationTitle("Transaction Details")
        .navigationBarTitleDisplayMode(.inline)
    }
}

// Formatter for the date
let itemFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .none
    return formatter
}()

// Formatter for currency
let currencyFormatter: NumberFormatter = {
    let formatter = NumberFormatter()
    formatter.numberStyle = .currency
    formatter.locale = Locale.current // Adjust the locale if necessary
    return formatter
}()



class CreditCard: Identifiable, ObservableObject {
    let id = UUID()
    @Published var name: String
    @Published var institution: String
    @Published var balance: Double
    @Published var limit: Double
    @Published var color: Color
    
    
    init(name: String, institution: String, balance: Double, limit: Double, color: Color)
    {
        self.name = name
        self.institution = institution
        self.balance = balance
        self.limit = limit
        self.color = color
    }
}

class BudgetGraphViewModel: ObservableObject {
    @Published var showingBudgetBars = true
    @Published var budgetCategories: [BudgetCategory]
    @Published var income: Double = 3000.0 // Example income value
    
    var totalSpent: Double {
        budgetCategories.map { $0.spent }.reduce(0, +)
    }
    
    init() {
        // Initialize the budget categories here
        budgetCategories = [
            // ... other categories'
            BudgetCategory(name: "Income", budget: 0.0, spent: 0.0),
            BudgetCategory(name: "Housing", budget: 1500.0, spent: 800.0),
            BudgetCategory(name: "Transport", budget: 300.0, spent: 200.0),
            BudgetCategory(name: "Utilities", budget: 200.0, spent: 150.0),
            BudgetCategory(name: "Groceries", budget: 400.0, spent: 250.0),
            BudgetCategory(name: "Health", budget: 250.0, spent: 100.0),
            BudgetCategory(name: "Wealth", budget: 600.0, spent: 300.0),
            BudgetCategory(name: "Entertainment", budget: 200.0, spent: 100.0),
            BudgetCategory(name: "Food", budget: 300.0, spent: 200.0),
            BudgetCategory(name: "Misc", budget: 150.0, spent: 550.0)
            // ... more categories
        ]
    
        updateCategories()
    }
    
    func updateCategories() {
        // Update the budget for each category except income
        let totalSpentExcludingIncome = budgetCategories.filter { $0.name != "Income" }.map { $0.spent }.reduce(0, +)
        if let incomeCategory = budgetCategories.first(where: { $0.name == "Income" }) {
            incomeCategory.budget = income
            incomeCategory.spent = totalSpentExcludingIncome
        }
    }
}

struct CreditCardSpendingGraph: View {
    var creditCards: [CreditCard]
    
    // Calculate the sum of all limits to represent 100% width in the graph
    var totalLimit: Double {
        creditCards.reduce(0) { $0 + $1.limit }
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            // Display the total credit limit
            Text("Total Credit Limit: \(currencyFormatter.string(from: NSNumber(value: totalLimit)) ?? "")")
                .font(.headline)
            
            // Create the graph using GeometryReader
            GeometryReader { geometry in
                HStack(alignment: .center, spacing: 4) {
                    ForEach(creditCards, id: \.id) { card in
                        VStack {
                            // Use ZStack to overlay the spent amount on top of the total limit
                            ZStack(alignment: .leading) {
                                // Display the total limit of the card
                                Rectangle()
                                    .fill(card.color.opacity(0.3)) // Use a transparent color for the unspent portion
                                    .frame(width: (geometry.size.width - (CGFloat(creditCards.count - 1) * 4)) * CGFloat(card.limit / totalLimit), height: 20)
                                
                                // Display the spent amount of the card
                                Rectangle()
                                    .fill(card.color) // Use the solid color for the spent portion
                                    .frame(width: (geometry.size.width - (CGFloat(creditCards.count - 1) * 4)) * CGFloat(card.balance / totalLimit), height: 20)
                            }
                            .cornerRadius(5)
                            
                            // Display the label below the bar, ensuring it's not cut off
                            Text("\(currencyFormatter.string(from: NSNumber(value: card.balance)) ?? "")")
                                .font(.caption)
                                .frame(width: (geometry.size.width - (CGFloat(creditCards.count - 1) * 4)) * CGFloat(card.limit / totalLimit), height: 20)
                                .background(Color.white)
                        }
                    }
                }
            }
            .frame(height: 40) // Increase the height if needed to fit labels
            
            // Display the legend
            LegendView(creditCards: creditCards)
        }
        .padding()
    }
}

struct LegendView: View {
    var creditCards: [CreditCard]
    
    var body: some View {
        HStack {
            ForEach(creditCards, id: \.id) { card in
                HStack {
                    Circle()
                        .fill(card.color)
                        .frame(width: 10, height: 10)
                    Text(card.name)
                }
            }
        }
    }
}

extension Array where Element == CreditCard {
    var totalCreditLimit: Double {
        reduce(0) { $0 + $1.limit }
    }
    
    var totalSpent: Double {
        reduce(0) { $0 + $1.balance }
    }
    
    var totalLimit: Double {
        Swift.max(totalCreditLimit - totalSpent, 0)
    }
}

struct BudgetBreakDownView: View {
    @Binding var budgetCategories: [BudgetCategory]

    var body: some View {
        ScrollView {
            VStack(spacing: 20) {
                ForEach(budgetCategories) { category in
                    BudgetBreakDownGraphView(budgetCategory: category)
                        .frame(height: 60)
                        .padding(.horizontal)
                }
            }
        }
    }
}

struct BudgetBreakDownGraphView: View {
    @ObservedObject var budgetCategory: BudgetCategory

    var body: some View {
        VStack {
            // First row for the title and total budget
            HStack {
                Text(budgetCategory.name)
                    .font(.headline)
                    .minimumScaleFactor(0.6)
                    .lineLimit(1)
                Spacer()
                Text("$\(budgetCategory.budget, specifier: "%.2f")")
                    .foregroundColor(.black)
                    .font(.headline)
                    .minimumScaleFactor(0.6)
                    .lineLimit(1)
            }
            .padding([.top, .leading, .trailing])
            
            // Second row for the graph
            GeometryReader { geometry in
                ZStack(alignment: .leading) {
                    Capsule()
                        .frame(width: geometry.size.width, height: 20)
                        .foregroundColor(Color.green.opacity(0.2))
                    
                    HStack(spacing: 0) {
                        Capsule()
                            .frame(width: calculateBarWidth(geometry: geometry, for: budgetCategory.spent), height: 20)
                            .foregroundColor(budgetCategory.spent > budgetCategory.budget ? Color.red : Color.orange)
                            .overlay(
                                Text("$\(budgetCategory.spent, specifier: "%.2f")")
                                    .font(.caption)
                                    .foregroundColor(.white)
                                    .padding(.leading, 4),
                                alignment: .leading
                            )
                        
                        if budgetCategory.spent < budgetCategory.budget {
                            Capsule()
                                .frame(width: calculateBarWidth(geometry: geometry, for: budgetCategory.budget - budgetCategory.spent), height: 20)
                                .foregroundColor(Color.green)
                        }
                    }
                }
            }
            .frame(height: 20)
            .padding([.leading, .trailing, .bottom])
        }
        .background(RoundedRectangle(cornerRadius: 10).fill(Color(UIColor.secondarySystemBackground)))
        .shadow(radius: 3)
    }
    
    private func calculateBarWidth(geometry: GeometryProxy, for amount: Double) -> CGFloat {
        let maxWidth = geometry.size.width
        let proportion = amount / budgetCategory.budget
        let spentWidth = proportion * maxWidth
        return proportion > 1.0 ? maxWidth : spentWidth
    }
}

struct CreditSpendingBreakdownView: View {
    var body: some View {
        Text("Credit breakdown view")
    }
}

struct BudgetGraphView: View {
    var showCreditSpendingView: Bool = true
    @StateObject var viewModel = BudgetGraphViewModel()
    
    var body: some View {
        // Removed ScrollView
        VStack {
            HStack {
                Button(action: {
                    viewModel.showingBudgetBars.toggle()
                }) {
                    Text("Toggle")
                }
                Text(viewModel.showingBudgetBars ? "Pie Chart" : "Line Graph")
            }
            
            if viewModel.showingBudgetBars {
                CreditCardSpendingGraph(creditCards: mockCreditCards)
                
                TransactionListView(transactions: mockTransactions)
                // Now the List will be able to manage its own scrolling
            } else {
                BudgetBreakDownView(budgetCategories: $viewModel.budgetCategories)
                // Other content as needed
            }
        }
        // If you need the entire VStack to be scrollable, including the toggle button and title, you can put the ScrollView here, outside of the conditional content.
    }
}

struct BudgetGraphView_Previews: PreviewProvider {
    static var previews: some View {
        BudgetGraphView()
    }
}

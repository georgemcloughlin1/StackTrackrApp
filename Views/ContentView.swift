//
//  Navigation.swift
//  StackTrackrApp
//
//  Created by George McLoughlin on 11/5/23.
//

import Foundation
import SwiftUI
struct ContentView: View {
    var body: some View {
        TabView {
            NetworthPreView()
                .tabItem {
                    Image(systemName: "dollarsign.circle")
                    Text("Networth")
                }
            
            BudgetPreView()
                .tabItem {
                    Image(systemName: "chart.bar.xaxis")
                    Text("Budget")
                    
                }
            
            SettingsView()
                .tabItem {
                    Image(systemName: "gear")
                    Text("Settings")
                }

        }
    }
}


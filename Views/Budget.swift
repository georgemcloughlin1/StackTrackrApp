//
//  Budget.swift
//  StackTrackrApp
//
//  Created by George McLoughlin on 11/5/23.
//

import Foundation
import SwiftUI
import FirebaseFirestoreSwift
import Firebase

struct BudgetPreView: View {
    @ObservedObject var viewModel = BudgetViewModel()
    var body: some View {
        VStack {
            if viewModel.hasBudgetAccounts {
                // Display networth details
            } else {
                Text("You have no linked budgeting accounts.")
                Button("Link Account") {
                    // Initiate Plaid account linking process
                }
            }
        }
        .onAppear {
            viewModel.fetchAccounts(userId: "yourUserId") // Replace with actual user ID
        }
    }
}


class BudgetViewModel: ObservableObject {
    @Published var accounts = [Account]()
    @Published var hasBudgetAccounts = false  // Changed to specifically track networth accounts

    func fetchAccounts(userId: String) {
        let firestore = Firestore.firestore()
        firestore.collection("users").document(userId).collection("accounts").getDocuments { [weak self] (snapshot, error) in
            if let error = error {
                print("Error getting accounts: \(error)")
                self?.hasBudgetAccounts = false
                return
            }

            if let snapshot = snapshot {
                self?.accounts = snapshot.documents.compactMap { document in
                    try? document.data(as: Account.self)
                }

                // Check specifically for 'networth' type accounts
                self?.hasBudgetAccounts = self?.accounts.contains(where: { $0.type == "budget" }) ?? false
            } else {
                self?.hasBudgetAccounts = false
            }
        }
    }
}

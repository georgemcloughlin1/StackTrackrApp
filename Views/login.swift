import SwiftUI
import FirebaseAuth // Make sure to import FirebaseAuth

struct Login: View {
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var loginError: String? // Added to handle login error messages
    @EnvironmentObject var sessionStore: SessionStore
    @State private var showingForgotPassword = false
    var body: some View {
        NavigationView {
            VStack {
                Text("Welcome Back!")
                    .font(.largeTitle)
                    .fontWeight(.semibold)
                    .padding(.bottom, 20)

                TextField("Username", text: $username)
                    .padding()
                    .background(Color(.systemGray6))
                    .cornerRadius(5)
                    .padding(.bottom, 10)
                
                SecureField("Password", text: $password)
                    .padding()
                    .background(Color(.systemGray6))
                    .cornerRadius(5)
                    .padding(.bottom, 20)
                
                // Login button with action to authenticate using Firebase
                Button(action: {
                    sessionStore.signIn(email: username, password: password)
                }) {
                    Text("Log In")
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 50)
                        .foregroundColor(.white)
                        .font(.system(size: 14, weight: .bold))
                        .background(Color.blue)
                        .cornerRadius(5)
                }
                .padding(.bottom, 10)

                if let loginError = loginError {
                    Text(loginError)
                        .foregroundColor(.red)
                        .padding()
                }
                
                // Forgot Password Button
                Button("Forgot Password?") {
                    // This will show the ForgotPasswordView as a sheet
                    self.showingForgotPassword = true
                }
                .foregroundColor(.blue)
                .padding()


                // Navigation link to go to the registration page
                NavigationLink(destination: RegisterPage()) {
                    Text("Register")
                        .foregroundColor(.blue)
                }
            }
            .padding()
            .navigationBarHidden(true)
            .sheet(isPresented: $showingForgotPassword) {
                // Present the ForgotPasswordView when showingForgotPassword is true
                ForgotPasswordView() // Make sure this view can dismiss itself or update the state
            }
                Spacer()
                
            
            }
            .padding()
            .navigationBarHidden(true)
        }
}

struct LoginPage_Previews: PreviewProvider {
    static var previews: some View {
        Login()
    }
}

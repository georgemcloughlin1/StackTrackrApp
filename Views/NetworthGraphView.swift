import SwiftUI
import Charts

// Enum for account categories
enum AccountCategory: String, CaseIterable, Hashable {
    case totalNetworth = "Total Networth"
    case liquidity = "Liquidity"
    case investments = "Investments"
    case retirement = "Retirement"
    case assets = "Assets"
}

// TimeSpan enum for different graph time spans
enum TimeSpan: String, CaseIterable, Hashable {
    case oneMonth = "1M", threeMonths = "3M", sixMonths = "6M", oneYear = "1Y", threeYears = "3Y", fiveYears = "5Y", all = "ALL"
}

// Financial account structure
struct FinancialAccount: Identifiable {
    let id = UUID()
    let accountName: String
    let category: AccountCategory
    var monthlyData: [Date: Double]

    // Computed properties for account data
    var balance: Double {
        guard let latestDate = monthlyData.keys.sorted().last else { return 0 }
        return monthlyData[latestDate] ?? 0
    }

    var percentageChange: Double {
        let sortedData = monthlyData.sorted { $0.key > $1.key }
        guard let latest = sortedData.first, let previous = sortedData.dropFirst().first else { return 0 }
        return ((latest.value - previous.value) / previous.value) * 100
    }
    var lastEntryDateFormatted: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd"
        return formatter.string(from: monthlyData.keys.sorted().last ?? Date())
    }
}

// MARK: - ViewModel

class GraphViewModel: ObservableObject {
    @Published var financialAccounts: [FinancialAccount] = []
    @Published var selectedCategory: AccountCategory = .totalNetworth // Default to Total Networth
    @Published var showingLineGraph = true
    @Published var selectedTimeframe: TimeSpan = .oneMonth {
        didSet {
            updateChartData()
        }
    }

    init() {
        // Initialize financial accounts with sample data
        self.financialAccounts = generateInitialAccounts()
        // Add total net worth account to the list
        let totalNetworthData = calculateTotalNetworthData()
        let totalNetworthAccount = FinancialAccount(accountName: "Total Networth",
                                                    category: .totalNetworth,
                                                    monthlyData: totalNetworthData)
        self.financialAccounts.insert(totalNetworthAccount, at: 0)
        normalizeData()
    }

    private func generateInitialAccounts() -> [FinancialAccount] {
        let accountNamesByCategory: [AccountCategory: [String]] = [
            .liquidity: ["Checking", "Savings"],
            .investments: ["Stocks", "Bonds"],
            .retirement: ["401K", "IRA"],
            .assets: ["Car", "House"]
        ]
        

        return accountNamesByCategory.flatMap { category, names in
            names.map { name in
                FinancialAccount(accountName: "\(name) Account",
                                 category: category,
                                 monthlyData: generateSampleData(for: category))
            }
        }
        
    }
    private func generateSampleData(for category: AccountCategory) -> [Date: Double] {
        var data: [Date: Double] = [:]
        let currentDate = Date()
        let calendar = Calendar.current

        for monthOffset in 0..<72 { // Generating 5 years of monthly data
            if let date = calendar.date(byAdding: .month, value: -monthOffset, to: currentDate) {
                let baseValue: Double = 1000 // Base value for sample data
                let fluctuation = Double.random(in: -300...300) // Random fluctuation
                data[date] = baseValue + fluctuation
            }
        }
//        print(data)
//        print("==================")
        return data
    }
    
    func aggregateDataByCategory() -> [AccountCategory: Double] {
        // Excluding 'Total Networth' category in the calculation
        let categories = AccountCategory.allCases.filter { $0 != .totalNetworth }

        var categoryTotals: [AccountCategory: Double] = [:]

        for category in categories {
            let totalForCategory = financialAccounts
                .filter { $0.category == category }
                .map { $0.balance }
                .reduce(0, +)
            categoryTotals[category] = totalForCategory
        }

        return categoryTotals
    }
    
    private func normalizeData() {
        for i in 0..<financialAccounts.count {
            financialAccounts[i].monthlyData = normalizeMonthlyData(financialAccounts[i].monthlyData)
        }
    }

    // Normalize Monthly Data
    private func normalizeMonthlyData(_ data: [Date: Double]) -> [Date: Double] {
        var normalizedData = [Date: Double]()
        let calendar = Calendar.current
        
        for (date, value) in data {
            let day = calendar.component(.day, from: date)
            var normalizedDate: Date
            
            if day <= 10 {
                // Normalize to the 1st of the month
                normalizedDate = calendar.date(from: calendar.dateComponents([.year, .month], from: date))!
            } else if day <= 20 {
                // Normalize to the 11th of the month
                normalizedDate = calendar.date(from: calendar.dateComponents([.year, .month, .day], from: date))!
                normalizedDate = calendar.date(byAdding: .day, value: 10 - day, to: normalizedDate)!
            } else {
                // Normalize to the 21st of the month
                normalizedDate = calendar.date(from: calendar.dateComponents([.year, .month, .day], from: date))!
                normalizedDate = calendar.date(byAdding: .day, value: 20 - day, to: normalizedDate)!
            }
            
            normalizedData[normalizedDate, default: 0] += value
        }
        
        return normalizedData
    }


    private func calculateTotalNetworthData() -> [Date: Double] {
        var totalNetworthData = [Date: Double]()
        let calendar = Calendar.current

        // Initialize most recent data for each group
        var mostRecentDataForGroup: [String: Double] = ["1-10": 0, "11-20": 0, "21-end": 0]

        // Process each account, excluding the total net worth account
        for account in financialAccounts where account.category != .totalNetworth {
            for (date, value) in account.monthlyData {
                let day = calendar.component(.day, from: date)
                let groupKey: String

                if day <= 10 {
                    groupKey = "1-10"
                } else if day <= 20 {
                    groupKey = "11-20"
                } else {
                    groupKey = "21-end"
                }

                // Determine the normalized date
                let components = calendar.dateComponents([.year, .month], from: date)
                let normalizedDate = calendar.date(from: components)!

                // Use the most recent data for the group if no data exists for the current date
                let effectiveValue = account.monthlyData[date] ?? mostRecentDataForGroup[groupKey]!

                // Update the total net worth for the normalized date
                totalNetworthData[normalizedDate, default: 0] += effectiveValue

                // Update the most recent data for the group
                mostRecentDataForGroup[groupKey] = effectiveValue
            }
        }

        return totalNetworthData
    }


    func filteredData(for account: FinancialAccount) -> [Date: Double] {
        let endDate = Date()
        let startDate: Date = {
            switch selectedTimeframe {
            case .oneMonth:
                return Calendar.current.date(byAdding: .month, value: -2, to: endDate)!
            case .threeMonths:
                return Calendar.current.date(byAdding: .month, value: -3, to: endDate)!
            case .sixMonths:
                return Calendar.current.date(byAdding: .month, value: -6, to: endDate)!
            case .oneYear:
                return Calendar.current.date(byAdding: .year, value: -1, to: endDate)!
            case .threeYears:
                return Calendar.current.date(byAdding: .year, value: -3, to: endDate)!
            case .fiveYears:
                return Calendar.current.date(byAdding: .year, value: -5, to: endDate)!
            case .all:
                return Calendar.current.date(byAdding: .year, value: -5, to: endDate)! // Assuming 5 years for 'ALL'
            }
        }()
        
        return account.monthlyData.filter { date, _ in
            date >= startDate && date <= endDate
        }
    }
    
    func filteredAggregateData(for category: AccountCategory) -> [Date: Double] {
        let endDate = Date()
        let startDate: Date = {
            switch selectedTimeframe {
            case .oneMonth:
                return Calendar.current.date(byAdding: .month, value: -1, to: endDate)!
            case .threeMonths:
                return Calendar.current.date(byAdding: .month, value: -3, to: endDate)!
            case .sixMonths:
                return Calendar.current.date(byAdding: .month, value: -6, to: endDate)!
            case .oneYear:
                return Calendar.current.date(byAdding: .year, value: -1, to: endDate)!
            case .threeYears:
                return Calendar.current.date(byAdding: .year, value: -3, to: endDate)!
            case .fiveYears:
                return Calendar.current.date(byAdding: .year, value: -5, to: endDate)!
            case .all:
                return Calendar.current.date(byAdding: .year, value: -5, to: endDate)! // Assuming 5 years for 'ALL'
            }
        }()

        return financialAccounts
            .filter { $0.category == category }
            .flatMap { $0.monthlyData }
            .filter { $0.key >= startDate && $0.key <= endDate }
            .reduce(into: [Date: Double]()) { (result, entry) in
                result[entry.key, default: 0] += entry.value
            }
    }


    private func updateChartData() {
        objectWillChange.send()
    }
}


struct LineGraphView: View {
    @ObservedObject var viewModel: GraphViewModel
    var financialAccount: FinancialAccount
    
    private var data: [(Date, Double)] {
        viewModel.filteredAggregateData(for: financialAccount.category)
            .map { ($0.key, $0.value) }
            .sorted { $0.0 < $1.0 }
    }

    var body: some View {
        Chart {
            ForEach(data, id: \.0) { (date, value) in
                LineMark(
                    x: .value("Date", date),
                    y: .value("Value", value)
                )
                AreaMark(
                   x: .value("Date", date),
                   y: .value("Value", value)
                )
                .foregroundStyle(Color.red.opacity(0.5))
            }
        }
    }
}

struct PieChartView: View {
    @ObservedObject var viewModel: GraphViewModel

    // This computed property will return the sum of balances for each category, excluding the 'Total Networth'
    private var categoryData: [(String, Double)] {
        // We filter out the 'Total Networth' category
        let filteredAccounts = viewModel.financialAccounts.filter { $0.category != .totalNetworth }
        // We create a dictionary to hold the sum of values for each category
        let sumsByCategory = Dictionary(grouping: filteredAccounts, by: { $0.category })
            .mapValues { $0.reduce(0) { $0 + $1.balance } }
            // Here we have a dictionary with keys of AccountCategory and values of the sum of balances

        // Now we can map this dictionary to an array of tuples and sort it by total balance
        return sumsByCategory
            .map { (key: $0.key.rawValue, value: $0.value) }
            .sorted { $0.value > $1.value }
    }

    var body: some View {
        VStack {
            if viewModel.selectedCategory == .totalNetworth {
                Chart(categoryData, id: \.0) { data in
                    SectorMark(
                        angle: .value("Amount", data.1)
                    )
                    .foregroundStyle(by: .value("Category", data.0))
                }
            } else {
                // If the selected category is not 'Total Networth', we show the accounts in that category
                let accountsInCategory = viewModel.financialAccounts.filter { $0.category == viewModel.selectedCategory }
                Chart(accountsInCategory, id: \.id) { account in
                    SectorMark(
                        angle: .value("Amount", account.balance)
                    )
                    .foregroundStyle(by: .value("Account Name", account.accountName))
                }
            }
        }
    }
}

struct CategoryListView: View {
    @ObservedObject var viewModel: GraphViewModel
    

    var body: some View {
        ForEach(AccountCategory.allCases, id: \.self) { category in
            VStack(alignment: .leading, spacing: 10) { // Add some spacing between categories
                Text(category.rawValue)
                    .font(.headline)
                    .padding([.top, .leading])

                RoundedRectangle(cornerRadius: 10)
                    .strokeBorder()
                    .frame(height: CGFloat(viewModel.financialAccounts.filter { $0.category == category }.count) * 70) // Calculate height based on number of accounts
                    .overlay(
                        VStack(spacing: 0) { // Remove spacing to allow dividers to appear correctly
                            ForEach(viewModel.financialAccounts.filter { $0.category == category }) { account in
                                HStack {
                                    VStack(alignment: .leading) {
                                        Text(account.accountName)
                                        Text("\(account.balance, specifier: "%.2f")")
                                            .font(.subheadline)
                                            .foregroundColor(.secondary)
                                    }
                                    Spacer()
                                    VStack(alignment: .trailing) {
                                        Text("\(abs(account.percentageChange), specifier: "%.2f")%")
                                            .font(.caption)
                                            .foregroundColor(account.percentageChange >= 0 ? .green : .red)
                                        Image(systemName: account.percentageChange >= 0 ? "arrow.up" : "arrow.down")
                                            .foregroundColor(account.percentageChange >= 0 ? .green : .red)
                                    }
                                    VStack(alignment: .trailing) {
                                        Text(account.lastEntryDateFormatted)
                                            .font(.caption)
                                            .foregroundColor(.secondary)
                                    }
                                }
                                .padding()
                                Divider() // Add a divider between accounts
                            }
                        }
                    )
                    .padding(.horizontal)
            }
        }
    }
}


struct NetworthGraphView: View {
    @StateObject var viewModel = GraphViewModel()
    var showNetworthPieChart: Bool = true
    
    var body: some View {
        ScrollView {
            VStack {
                HStack {
                    Text(viewModel.selectedCategory.rawValue)
                        .font(.title)
                        .padding()
                    Spacer()
                    Button(action: {
                        viewModel.showingLineGraph.toggle()
                    }) {
                        Text(viewModel.showingLineGraph ? "Pie Chart" : "Line Graph")
                            .padding()
                    }
                }

                if viewModel.showingLineGraph {
                    if let financialAccount = viewModel.financialAccounts.first(where: { $0.category == viewModel.selectedCategory }) {
                        LineGraphView(viewModel: viewModel, financialAccount: financialAccount)

                        Picker("Time Span", selection: $viewModel.selectedTimeframe) {
                            ForEach(TimeSpan.allCases, id: \.self) {
                                Text($0.rawValue)
                            }
                        }
                        .pickerStyle(SegmentedPickerStyle())
                        .padding()
                    } else {
                        Text("No data available for this category.")
                    }
                } else {
                    PieChartView(viewModel: viewModel)
                }

                Picker("Category", selection: $viewModel.selectedCategory) {
                    ForEach(AccountCategory.allCases, id: \.self) {
                        Text($0.rawValue)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding()
                
                CategoryListView(viewModel: viewModel)

            }
        }
    }
}

struct NetworthGraphView_Previews: PreviewProvider {
    static var previews: some View {
        NetworthGraphView()
    }
}


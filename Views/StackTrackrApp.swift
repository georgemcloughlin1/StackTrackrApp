//
//  StackTrackrAppApp.swift
//  StackTrackrApp
//
//  Created by George McLoughlin on 11/3/23.
//

import SwiftUI
import FirebaseCore

@main
struct StackTrackrAppApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    @StateObject private var sessionStore = SessionStore()

    var body: some Scene {
        WindowGroup {
            if sessionStore.isLoggedIn {
                ContentView().environmentObject(sessionStore)
            } else {
                Login().environmentObject(sessionStore)
            }
        }
    }
}



import SwiftUI
import FirebaseAuth

// Enum to manage the active view state
enum ActiveView {
    case emailSent, success, other
}

// Main View for Forgot Password Process
struct ForgotPasswordView: View {
    @State private var activeView: ActiveView = .emailSent

    var body: some View {
        VStack {
            switch activeView {
            case .emailSent:
                EmailSentView(activeView: $activeView)
            case .success:
                SuccessView(activeView: $activeView)
            case .other:
                Text("Current")
            }
        }
        .padding()
    }
}

struct EmailSentView: View {
    @Binding var activeView: ActiveView
    @State private var emailAddress: String = ""
    @State private var showAlert = false
    @State private var alertMessage: String = ""
    @EnvironmentObject var sessionStore: SessionStore
    @State private var exists: Bool?

    var body: some View {
        VStack {
            Group {
                if sessionStore.checkIfUserIsLoggedIn() {
                    // If the user is logged in, show a non-editable text with their email
                    Text(emailAddress)
                        .padding()
                } else {
                    // If the user is not logged in, show an editable text field to enter the email
                    TextField("Email Address", text: $emailAddress)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .padding()
                }
            }
            
            Button("Send Reset Code") {
                sessionStore.checkEmailExists(email: $emailAddress.wrappedValue) { exists in
                    if exists {
                        print("Email exists.")
                        sessionStore.sendPasswordReset(email: emailAddress) { error in
                            if let error = error {
                                // If there is an error, update the state variables to show the alert
                                self.alertMessage = error.localizedDescription
                                self.showAlert = true
                            } else {
                                // No error, proceed to the success view
                                self.activeView = .success
                            }
                        }
                    } else {
                        self.alertMessage = "The email does not have an associated account."
                        self.showAlert = true
                    }
                }
            }
            .padding()
        }
        .alert(isPresented: $showAlert) {
            Alert(title: Text("Error"), message: Text(alertMessage), dismissButton: .default(Text("OK")))
        }
        .onAppear {
            // Set the email address once when the view appears
            if sessionStore.checkIfUserIsLoggedIn() {
                emailAddress = Auth.auth().currentUser?.email ?? ""
            }
        }
    }
}

// Success View
struct SuccessView: View {
    @Binding var activeView: ActiveView
    @EnvironmentObject var sessionStore: SessionStore

    var body: some View {
        VStack {
            Text("Your password reset link has been successfully sent.")
                .multilineTextAlignment(.center)
            
            if sessionStore.checkIfUserIsLoggedIn() {
                NavigationLink(destination: SettingsView()) {
                    Text("Back to Settings")
                        .padding()
                }
            } else {
                NavigationLink(destination: Login()) {
                    Text("Go Home")
                        .padding()
                }
            }

        }
    }
}

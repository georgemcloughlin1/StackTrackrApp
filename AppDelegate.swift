//
//  AppDelegate.swift
//  StackTrackrApp
//
//  Created by George McLoughlin on 11/29/23.
//

import Foundation
import SwiftUI
import FirebaseCore

class AppDelegate: NSObject, UIApplicationDelegate {
 func application(_ application: UIApplication,
                 didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
   FirebaseApp.configure()
   return true
 }
}

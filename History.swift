//
//  History.swift
//  StackTrackrApp
//
//  Created by George McLoughlin on 11/26/23.
//

import Foundation
import FirebaseFirestoreSwift
import Firebase

struct HistoryEntry: Codable, Identifiable {
    @DocumentID var id: String?
    var timestamp: Timestamp
    var balance: Double
}


func addHistoryEntry(userId: String, accountId: String, entry: HistoryEntry) {
    let firestore = Firestore.firestore()
    let historyRef = firestore.collection("users").document(userId)
                                .collection("accounts").document(accountId)
                                .collection("history").document()

    do {
        try historyRef.setData(from: entry)
    } catch let error {
        print("Error adding history entry: \(error)")
    }
}

import SwiftUI
import FirebaseAuth
import Combine
import FirebaseFirestore

class SessionStore: ObservableObject {
    // Published property to track if the user is logged in or not.
    @Published var isLoggedIn: Bool = false
    // Published property to track any authentication error messages.
    @Published var authError: String?
    // This property will hold a handle to the Firebase listener.
    private var authStateDidChangeListenerHandle: AuthStateDidChangeListenerHandle?

    init() {
        // Immediately start listening to auth state changes.
        self.listenAuthenticationState()
    }

    // Method to listen to Firebase Auth state changes.
    func listenAuthenticationState() {
        authStateDidChangeListenerHandle = Auth.auth().addStateDidChangeListener { _, user in
            self.isLoggedIn = user != nil
        }
    }

    // Method to sign in a user.
    func signIn(email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] result, error in
            if let error = error {
                // Handle and display error.
                self?.authError = error.localizedDescription
            }
        }
    }

    // Method to sign out a user.
    func signOut() {
        do {
            try Auth.auth().signOut()
            self.isLoggedIn = false
        } catch {
            // Handle and display error.
            self.authError = error.localizedDescription
        }
    }

    // Method to sign up a new user.

    func signUp(email: String, password: String, displayName: String, phoneNumber: String) {
        Auth.auth().createUser(withEmail: email, password: password) { [weak self] result, error in
            if let error = error {
                self?.authError = error.localizedDescription
                return
            }

            guard let userId = result?.user.uid else { return }

            // Creating a Firestore entry in userInfo
            let firestore = Firestore.firestore()
            let userRef = firestore.collection("users").document(userId)
            let currentTime = Timestamp()

            let userData: [String: Any] = [
                "userID": userId,                           // User's unique identifier
                "email": email,                             // User's email address
                "displayName": displayName,                 // User's display name
                "profilePhotoURL": "",                      // URL to user's profile photo (empty if not available)
                "createdAt": currentTime,                   // Timestamp of account creation
                "updatedAt": currentTime,                   // Timestamp of the last update to user info
                "lastLogin": currentTime,                   // Timestamp of the last login
                "phoneNumber": phoneNumber,                // User's phone number (empty if not available)
                "isEmailVerified": false,                   // Flag for email verification status
                "preferredCurrency": "USD",                 // Preferred currency for transactions
                "locale": "en_US",                          // Localization setting (e.g., en_US, de_DE)
                "settings": [:]                             // Map for user-specific settings (empty initially)
            ]

            userRef.setData(userData) { error in
                if let error = error {
                    // Handle error in setting Firestore data
                    print("Error setting user data: \(error.localizedDescription)")
                } else {
                    // User data successfully written to Firestore
                    print("User data added to Firestore")
                }
            }
        }
    }
    
    func sendPasswordReset(email: String, completion: @escaping (Error?) -> Void) {
        Auth.auth().sendPasswordReset(withEmail: email) { error in
            if let error = error {
                // Call the completion handler with the error
                completion(error)
            } else {
                // Call the completion handler with nil to indicate success
                completion(nil)
            }
        }
    }
    
    func checkIfUserIsLoggedIn() -> Bool {
        return Auth.auth().currentUser != nil
    }


    func checkEmailExists(email: String, completion: @escaping (Bool) -> Void) {
        Auth.auth().fetchSignInMethods(forEmail: email) { (signInMethods, error) in
            if let error = error {
                print("Error checking email: \(error.localizedDescription)")
                completion(false)
            } else if let signInMethods = signInMethods, !signInMethods.isEmpty {
                // Email exists since there are sign-in methods available for it.
                completion(true)
            } else {
                // Email does not exist in Firebase Auth
                completion(false)
            }
        }
    }

    // Call this method to unregister the listener when the object deinitializes.
    deinit {
        if let handle = authStateDidChangeListenerHandle {
            Auth.auth().removeStateDidChangeListener(handle)
        }
    }
}

